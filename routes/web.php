<?php

use App\Http\Controllers\Site\HomeController; # don't forgot to add this
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Painel\QuizPainelController;
use App\Http\Controllers\Site\ThemeController;
use App\Http\Controllers\Site\QuizController;
use App\Http\Controllers\Site\ContactController;
use App\Http\Controllers\Painel\ThemePainelController;
use App\Http\Controllers\Painel\HomePainelController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::namespace('Site')->group(function () {
    Route::get('/', [HomeController::class, '__invoke'])->name('site.home');//Show home website
    
    Route::get('/clear-cache-all', function() {
        Artisan::call('cache:clear');
      
        dd("Cache Clear All");
    });

    Route::get('/phpinfo', [HomeController::class, 'phpinfo'])->name('painel.phpinfo');

    Route::get('/contato', [ContactController::class, 'index'])->name('site.contato');
    Route::post('/contato', [ContactController::class, 'index'])->name('site.contato.form');
    
    Route::get('/temas', [ThemeController::class, 'index'])->name('site.temas');//Show themes
    Route::get('/tema/{slug}', [ThemeController::class, 'show']);//Show list the of quiz avaliables
    
    Route::get('/quiz/finalizado/{slug}', [QuizController::class, 'thanks'])->name('site.thanks');//Result quizz
    
    Route::get('/quiz/{slug}', [QuizController::class, 'show'])->name('site.quiz');//Show especific quiz
    Route::post('/quiz/{slug}', [QuizController::class, 'form'])->name('site.quiz.form');//Show especific quiz

});


Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::namespace('Painel')->group(function () {
    Route::get('/painel/', [HomePainelController::class, 'index'])->name('painel.index');

    // Routes Temas
    Route::get('/painel/temas', [ThemePainelController::class, 'index'])->name('painel.theme');
    Route::post('/painel/temas', [ThemePainelController::class, 'form'])->name('painel.theme.form');
    
    Route::get('/painel/temas/novo', [ThemePainelController::class, 'create'])->name('painel.theme.create');
    Route::post('/painel/temas/novo', [ThemePainelController::class, 'store'])->name('painel.theme.store');
    
    Route::get('/painel/temas/editar/{id}', [ThemePainelController::class, 'show'])->name('painel.theme.show');
    Route::post('/painel/temas/editar/{id}', [ThemePainelController::class, 'update'])->name('painel.theme.update');

    // Show quizzes and filter
    Route::get('/painel/quiz', [QuizPainelController::class, 'index'])->name('painel.quizzes');
    Route::post('/painel/quiz', [QuizPainelController::class, 'form'])->name('painel.quiz.searchQuizByName');
    
    //create quiz
    Route::get('/painel/quiz/novo', [QuizPainelController::class, 'create'])->name('painel.quiz.create');
    Route::post('/painel/quiz/novo', [QuizPainelController::class, 'store'])->name('painel.quiz.store');
    
    //Show quiz specific
    Route::get('/painel/quiz/editar/{id}', [QuizPainelController::class, 'show'])->name('painel.quiz.show');

    //Update quiz
    Route::post('/painel/quiz/editar/{id}', [QuizPainelController::class, 'update'])->name('painel.quiz.update');  

    //Update question
    Route::post('/painel/quiz/editar/pertunta/{id}', [QuizPainelController::class, 'updateQuestion'])->name('painel.quiz.updateQuestion');   
    
    //Create and Update results the of quiz
    Route::post('/painel/quiz/editar/criar-resultado/{id}', [QuizPainelController::class, 'createResult'])->name('painel.quiz.createResult');
    Route::post('/painel/quiz/editar/editar-resultado/{id}', [QuizPainelController::class, 'updateResult'])->name('painel.quiz.updateResult');
    
    
    //AJAX
    Route::get('/painel/ajx/quiz/findQuestionById/', [QuizPainelController::class, 'findQuestionById'])->name('painel.quiz.findQuestionById');
    Route::get('/painel/ajx/quiz/findResultById/', [QuizPainelController::class, 'findResultById'])->name('painel.quiz.findResultById');
    

});