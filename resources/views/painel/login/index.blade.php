<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Painel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

    <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('painel/bootstrap/css/bootstrap.min.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('painel/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/themify-icons/themify-icons.css')}}">

    <!-- Chartist CSS -->
    <link rel="stylesheet" href="{{asset('painel/plugins/chartist-js/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('painel/plugins/chartist-js/chartist-plugin-tooltip.css')}}">

    <meta name="url_admin" content="{{URL::to('/painel/')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        @if ($message = Session::get('message'))
        <div class="alert alert-{{$message['type']}} alert-dismissible fade show" role="alert">{{$message['text']}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
        @endif
        <div class="login-box-body">
            <h3 class="login-box-msg">Login</h3>
            <form action="{{route('painel.login.form')}}" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input type="email" class="form-control sty1" name="email" placeholder="Email">
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control sty1" name="password" placeholder="Senha">
                </div>
                <div>
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <!-- <label>
                                <input type="checkbox">
                                Remember Me </label> -->
                            <a href="javscript:void(0)" class="pull-right"><i class="fa fa-lock"></i> Esqueceu a senha?</a>
                        </div>
                    </div>
                    <div class="col-xs-4 m-t-1">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                    </div>
                </div>
            </form>
            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                    Facebook</a> <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                    Google+</a>
            </div> -->

            <div class="m-t-2">Não tem conta? <a href="{{URL::to('/painel/casdatro')}}" class="text-center">Inscrever-se</a></div>
        </div>
    </div>

    <script src="{{asset('painel/js/jquery.min.js')}}"></script>
    <script src="{{asset('painel/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('painel/js/niche.js')}}"></script>

</body>
<style>
    html {
        background: unset;
    }
</style>

</html>