<div class="modal fade" id="formUpdateResult" data-backdrop="static">
    <div class="modal-dialog formUpdateResult modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title palavra">Atualizar condição para o resultado</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" novalidate="novalidate">
                @csrf 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value=""/>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <span class="titleField">Condição de resultado</span>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div>
                                        <input type="number" name="from" class="form-control" value="0" min="0" required="required" placeholder="De" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div>
                                        <input type="number" name="to" class="form-control" value="0" min="0" required="required" placeholder="Até" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-9">
                            <div>
                                <span class="titleField">Título</span>
                                <input type="text" name="title" class="form-control"  required="required" placeholder="Título" />
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div>
                                <span class="titleField">Texto</span>
                                <textarea class="form-control form-control" required="required"  name="description"></textarea>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <div>
                                <span class="titleField">Título do botão</span>
                                <input type="text" name="textButton" id="textButton" class="form-control configButton" placeholder="Texto do botão" />
                            </div>
                        </div>
                        <div class="form-group col-md-9">
                            <div>
                                <span class="titleField">Link do botão</span>
                                <input type="text" name="linkButton" id="linkButton" class="form-control configButton" placeholder="Link do botão" />
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div>
                                <input type="color" id="colorButton" name="colorButton" class="configButton" value="#ffffff">
                                <span for="colorButton">Cor do texto botão</span>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <div>
                                <input type="color" id="backgroundButton" name="backgroundButton" class="configButton" value="#001eff">
                                <span for="backgroundButton">Cor do botão</span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div>
                                <a class="btn" id="btnSite" href="javascript:void(0)" target="_blank" >Texto do botão</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <input type="button" name="action" value="Atualizar resultado" id="btnUpdateResult" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>