<!-- MODAL FOR ADD NEW QUESTION FOR OF QUIZ -->
<div class="modal fade" id="formUpdateQuestion" data-backdrop="static">
    <div class="modal-dialog modal-xl formUpdateQuestion">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Atualizar pergunta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value=""/>
                <div class="modal-body">
                    <div class="row" id="clonarQuestion">
                        <div class="form-group col-md-12 mb-4">
                            <div>
                                <span class="titleField">Pergunta</span>
                                <input type="text" name="question" class="form-control" placeholder="Pergunta" maxlength="250" />
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div>
                                <input type="number" name="weight1" class="form-control" required="required"   placeholder="Peso A" min="0" value="0" />
                            </div>
                        </div>
                        <div class="form-group col-md-10  mb-4">
                            <div>
                                <span class="titleField">Alternativa A</span>
                                <input type="text" name="alternative1" class="form-control" maxlength="250"  required="required"  placeholder="Alternativa A" />
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div>
                                <input type="number" name="weight2" class="form-control"  required="required"  placeholder="Peso B" min="0" value="0" />
                            </div>
                        </div>
                        <div class="form-group col-md-10  mb-4">
                            <div>
                                <span class="titleField">Alternativa B</span>
                                <input type="text" name="alternative2" class="form-control"  required="required"  maxlength="250" placeholder="Alternativa B" />
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div>
                                <input type="number" name="weight3" class="form-control" placeholder="Peso C" min="0" value="0" />
                            </div>
                        </div>
                        <div class="form-group col-md-10  mb-4">
                            <div>
                                <span class="titleField">Alternativa C</span>
                                <input type="text" name="alternative3" class="form-control" maxlength="250" placeholder="Alternativa C" />
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div>
                                <input type="number" name="weight4" class="form-control" placeholder="Peso D" min="0" value="0" />
                            </div>
                        </div>
                        <div class="form-group col-md-10  mb-4">
                            <div>
                                <span class="titleField">Alternativa D</span>
                                <input type="text" name="alternative4" class="form-control" maxlength="250" placeholder="Alternativa D" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <input type="button" name="action" value="Atualizar pergunta" id="btnUpdateQuestion" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>