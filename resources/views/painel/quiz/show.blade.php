@extends('adminlte::page')

@section('title', 'Editar')

@section('content_header')
<h1>Editar</h1>
@stop

@section('content')

@if ($message = Session::get('message'))
<div class="alert alert-{{$message['type']}} alert-dismissible fade show w-100" role="alert"> {{$message['text']}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="col-md-12">
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="configurations-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="configurations-home-tab" data-toggle="pill" href="#configurations-home" role="tab" aria-controls="configurations-home" aria-selected="false">Configurações</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="questions-alternatives-tab" data-toggle="pill" href="#questions-alternatives" role="tab" aria-controls="questions-alternatives" aria-selected="false">Perguntas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="results-tab" data-toggle="pill" href="#results" role="tab" aria-controls="results" aria-selected="true">Condições do Resultado</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="configurations-tabContent">
                <div class="tab-pane fade active show" id="configurations-home" role="tabpanel" aria-labelledby="configurations-home-tab">
                    <form method="post" enctype="multipart/form-data" action="{{route('painel.quiz.update', $quiz->id)}}" class="form-horizontal form-material row">
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <span class="titleField">Título </span>
                                        <div>
                                            <input class="form-control form-control-line" type="text" required="required" Placeholder="Título do QUIZ" name="name" value="{{$quiz->name}}">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <span class="titleField">Tema </span>
                                        <div>
                                            <select name="theme" class="form-control">
                                                <?php foreach ($themes as $theme) : ?>
                                                    <option value="<?= $theme->id ?>"><?= $theme->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <span class="titleField">Ativo ? </span>
                                        <div>
                                            <label>Não</label>
                                            <input type="range" min="0" max="1" step="1" name="active" value="{{$quiz->active}}" style="width:50px">
                                            <label>Sim</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <span class="titleField">Diponível no site ? </span>
                                        <div>
                                            <label>Não</label>
                                            <input type="range" min="0" max="1" step="1" name="show_in_site" value="{{$quiz->show_in_site}}" style="width:50px">
                                            <label>Sim</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <span class="titleField">Descrição</span>
                                        <div>
                                            <textarea class="form-control form-control" name="description" maxlength="250">{{$quiz->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12">
                                    <span class="titleField">Imagem</span>
                                    <div class="row">
                                        <input type="file" class="form-control mb-4" name="image">
                                        <img src="<?= asset('site/img/quiz/' . $quiz->image) ?>" class="m-auto" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="form-group col-md-12">
                                <span class="titleField">Colocar código em HEAD (Ex: pixel facebook)</span>
                                <div>
                                    <textarea class="form-control form-control" name="pixel">{{$quiz->pixel}}</textarea>
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                <input type="submit" class="form-control btn-primary" name="action" value="Atualizar quiz" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="questions-alternatives" role="tabpanel" aria-labelledby="questions-alternatives-tab">
                    <div id="accordion" class="mb-4">
                        <div class="card ">
                            <?php foreach ($questions as $question) : ?>
                                <?php
                                $alternative1 = json_decode($question->alternative1);
                                $alternative2 = json_decode($question->alternative2);
                                $alternative3 = json_decode($question->alternative3);
                                $alternative4 = json_decode($question->alternative4);
                                ?>
                                <div class="card-header">
                                    <h4 class="card-title title-question">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $question->id ?>">
                                            <?= json_decode($question->question)->question ?>
                                        </a>

                                    </h4>
                                    <div class="btn-actions">
                                        <a href="#" class="mr-1 open-form-edit-question" data-idQuestion="<?= $question->id ?>"><i class="fas fa-edit"></i></a> |
                                        <a href="#" class="ml-1"><i class="fas fa-trash-alt text-red"></i></a>
                                    </div>
                                </div>

                                <div id="collapse<?= $question->id ?>" class="panel-collapse collapse in">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            <!-- <li class="list-group-item active" aria-current="true">An active item</li> -->
                                            <li class="list-group-item"><strong>A) Peso: (<?= $alternative1->weight ?>)</strong> <?= $alternative1->alternative ?></li>
                                            <li class="list-group-item"><strong>B) Peso: (<?= $alternative2->weight ?>)</strong> <?= $alternative2->alternative ?></li>
                                            <li class="list-group-item"><strong>C) Peso: (<?= $alternative3->weight ?>)</strong> <?= $alternative3->alternative ?></li>
                                            <li class="list-group-item"><strong>D) Peso: (<?= $alternative4->weight ?>)</strong> <?= $alternative4->alternative ?></li>
                                        </ul>

                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    @include('painel.quiz.formNewQuestion')
                </div>
                <div class="tab-pane fade" id="results" role="tabpanel" aria-labelledby="results-tab">
                    <div id="accordion" class="mb-4">
                        <div class="card ">
                            <?php foreach ($results as $result) : ?>
                                <div class="card-header">
                                    <h4 class="card-title title-question">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $result->id ?>">
                                            Pontuação <?= $result->from . " - " . $result->to ?> - <?= $result->title ?>
                                        </a>

                                    </h4>
                                    <div class="btn-actions">
                                        <a href="#" class="mr-1 open-form-edit-result" data-idResult="<?= $result->id ?>"><i class="fas fa-edit"></i></a> |
                                        <a href="#" class="ml-1"><i class="fas fa-trash-alt text-red"></i></a>
                                    </div>
                                </div>

                                <div id="collapse<?= $result->id ?>" class="panel-collapse collapse in">
                                    <div class="card-body">
                                        <?= $result->description ?>
                                        <hr />
                                        <a href="<?= $result->linkButton ?>" target="_blank" id="viewBtnResult"><?= $result->textButton ?></a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    @include('painel.quiz.formNewResultQuiz')
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.card -->



@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<style>
    .titleField {
        font-size: 14px;
        font-weight: 800;
        color: #9c9c9c;
        line-height: 0px;
        top: -6px;
        position: absolute;
    }

    .btn-actions {
        float: right;
    }

    .viewBtnResult {
        color: "<?= !empty($result->colorButton) ? $result->colorButton : "#001eff" ?>";
        background: "<?= !empty($result->backgroundButton) ? $result->backgroundButton : "#001eff" ?>";
    }
</style>
@stop

@section('js')
<script>
    //Using in the file 'painel.quiz.formUpdateQuestion'
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Opens modal to edit question
        $(".open-form-edit-question").on("click", function() {
            var idQuestion = $(this).attr("data-idquestion");
            $.ajax({
                type: 'get',
                url: "{{route('painel.quiz.findQuestionById')}}",
                data: {
                    id: idQuestion
                },
                success: function(data) {
                    var idQuestion = data.id;
                    var idQuiz = data.quiz_id;
                    var question = JSON.parse(data.question);
                    var alternative1 = JSON.parse(data.alternative1);
                    var alternative2 = JSON.parse(data.alternative2);
                    var alternative3 = JSON.parse(data.alternative3);
                    var alternative4 = JSON.parse(data.alternative4);

                    $("#formUpdateQuestion input[name=question] ").val(question.question)
                    $("#formUpdateQuestion input[name=weight1] ").val(alternative1.weight)
                    $("#formUpdateQuestion input[name=weight2] ").val(alternative2.weight)
                    $("#formUpdateQuestion input[name=weight3] ").val(alternative3.weight)
                    $("#formUpdateQuestion input[name=weight4] ").val(alternative4.weight)

                    $("#formUpdateQuestion input[name=alternative1] ").val(alternative1.alternative)
                    $("#formUpdateQuestion input[name=alternative2] ").val(alternative2.alternative)
                    $("#formUpdateQuestion input[name=alternative3] ").val(alternative3.alternative)
                    $("#formUpdateQuestion input[name=alternative4] ").val(alternative4.alternative)

                    $("#formUpdateQuestion input[name=id]").val(idQuestion)
                    $("#formUpdateQuestion form").attr("action", "{{route('painel.quiz.updateQuestion', '')}}")
                    $("#formUpdateQuestion").modal('show');
                }
            });
        });

        //Submit issue edit form
        $("#btnUpdateQuestion").on("click", function() {
            var idQuestion = $("#formUpdateQuestion input[name=id]").val()
            var action = $("#formUpdateQuestion form").attr("action");
            var actionComplete = action + "/" + idQuestion
            $("#formUpdateQuestion form").attr("action", actionComplete);
            $("#formUpdateQuestion form").submit();
        });

        // _______________________________________________________________________



        //Opens modal to edit result
        $(".open-form-edit-result").on("click", function() {
            var idResult = $(this).attr("data-idresult");
            $("#formUpdateResult").modal('show');
            $.ajax({
                type: 'get',
                url: "{{route('painel.quiz.findResultById')}}",
                data: {
                    id: idResult
                },
                success: function(data) {

                    $("#formUpdateResult input[name=from] ").val(data.from)
                    $("#formUpdateResult input[name=to] ").val(data.to)
                    $("#formUpdateResult input[name=title] ").val(data.title)
                    $("#formUpdateResult textarea[name=description] ").val(data.description)
                    $("#formUpdateResult input[name=textButton] ").val(data.textButton)
                    $("#formUpdateResult input[name=linkButton] ").val(data.linkButton)
                    $("#formUpdateResult input[name=colorButton] ").val(data.colorButton)
                    $("#formUpdateResult input[name=backgroundButton] ").val(data.backgroundButton)
                    $("#formUpdateResult input[name=id]").val(data.id)
                    $("#formUpdateResult form").attr("action", "{{route('painel.quiz.updateResult', '')}}")
                    $("#formUpdateResult").modal('show');
                }
            });
        });



        //Configure form the of add new result
        $("#btnUpdateResult").on("click", function(e) {
            // if($("#formUpdateResult input[name=title] ").val() == ''){
            //     console.log('campo obrigatorio');
            // }
            // $('#formUpdateResult').validate();
            var idResult = $("#formUpdateResult input[name=id]").val()
            var actionComplete = "{{route('painel.quiz.updateResult','')}}/" + idResult
            $("#formUpdateResult form").attr("action", actionComplete);
            $("#formUpdateResult form").submit();
        });

        function validaCampos()
        {
            $('#formUpdateResult').validate({
                rules: {
                    title: {
                        required: true,
                    },
                },
                messages: {
                    title: {
                        required: "Título obrigatório",
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        }

        //Configure button do form the of result
        $(".configButton").on("change", function() {
            var $textButton = $("#textButton").val();
            var $linkButton = $("#linkButton").val();
            var $colorButton = $("#colorButton").val();
            var $backgroundButton = $("#backgroundButton").val();
            var $btnSite = $("#btnSite");

            console.log($textButton)
            $btnSite.html($textButton);
            $btnSite.attr("href", $linkButton);
            $btnSite.css("color", $colorButton);
            $btnSite.css("background-color", $backgroundButton);
        });
    });
</script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>

@stop