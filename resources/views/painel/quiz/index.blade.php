@extends('adminlte::page')

<?php

use Illuminate\Support\Str;
?>

@section('title', 'Quizz')

@section('content_header')
<h1>Quizz</h1>
@stop

@section('content')
<div class="info-box row">
    <div class="col-md-12">
        <form action="{{ route('painel.quiz.searchQuizByName') }}" method="post" class="row">
            @csrf
            <div class="form-group col-xs-8 col-md-4 col-lg-4">
                <label>Título</label>
                <div>
                    <input type="text" class="form-control" name="search" placeholder="Digite o nome do quiz" />
                </div>
            </div>
            <div class="form-group col-xs-4 col-md-6 col-lg-6">
                <label></label>
                <div>
                    <input type="submit" class="btn btn-primary" name="action" value="Buscar" />
                </div>
            </div>
        </form>
        <hr>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="example2" class="table table-bordered table-hover no-wrap">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Tema</th>
                        <th>Ativo</th>
                        <th>Editar</th>
                        <th>Deletar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($quizzes as $quiz) : ?>
                        <tr>
                            <td><?= $quiz->name ?></td>
                            <td><?= $quiz->theme ?></td>
                            <td><?= $quiz->active == 0 ? "Desativado" : "Ativo" ?></td>
                            <td><a href="{{URL::to('/painel/quiz/editar/'.$quiz->id)}}"><i class="fa fa-eye"></i></a></td>
                            <td><a href="#"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" style="text-align: right;font-weight:600"><?= count($quizzes) ?> quizzes localizados </td>
                    </tr>
                </tfoot>
            </table>
            <hr>
            {{ $quizzes->links() }}
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop