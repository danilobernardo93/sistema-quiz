@extends('adminlte::page')

<?php

use Illuminate\Support\Str;
?>

@section('title', 'Criar Quizz')

@section('content_header')
<h1>Criar Quizz</h1>
@stop

@section('content')

@if ($message = Session::get('message'))
<div class="alert alert-{{$message['type']}} alert-dismissible fade show w-100" role="alert"> {{$message['text']}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form method="post" enctype="multipart/form-data" action="{{route('painel.quiz.store')}}" class="form-horizontal form-material row">
    @csrf
    <div class="col-md-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="configurations-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="configurations-home-tab" data-toggle="pill" href="#configurations-home" role="tab" aria-controls="configurations-home" aria-selected="false">Configurações</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="configurations-tabContent">
                    <div class="tab-pane fade  active show" id="configurations-home" role="tabpanel" aria-labelledby="configurations-home-tab">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <span class="titleField">Título </span>
                                        <div>
                                            <input class="form-control form-control-line" type="text" required="required" Placeholder="Título do QUIZZ" name="name">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <span class="titleField">Tema </span>
                                        <div>
                                            <select name="theme" class="form-control">
                                                <?php foreach ($themes as $theme) : ?>
                                                    <option value="<?= $theme->id ?>"><?= $theme->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <span class="titleField">Ativo ? </span>
                                        <div>
                                            <label>Não</label>
                                            <input type="range" min="0" max="1" step="1" name="active" style="width:50px">
                                            <label>Sim</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <span class="titleField">Diponível no site ? </span>
                                        <div>
                                            <label>Não</label>
                                            <input type="range" min="0" max="1" step="1" name="show_in_site" style="width:50px">
                                            <label>Sim</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <span class="titleField">Descrição</span>
                                        <div>
                                            <textarea class="form-control form-control" name="description"  maxlength="250"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="form-group col-xs-12 col-md-12 col-lg-12">
                                    <span class="titleField">Imagem</span>
                                    <div class="row">
                                        <input type="file" class="form-control" name="image">
                                        Preview imagem
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="form-group col-md-12">
                                <span class="titleField">Colocar código em HEAD (Ex: pixel facebook)</span>
                                <div>
                                    <textarea class="form-control form-control" name="pixel"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="submit" class="form-control btn-primary" name="action" value="Salvar" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
    </div>
</form>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<style>
    .titleField {
        font-size: 14px;
        font-weight: 800;
        color: #9c9c9c;
        line-height: 0px;
        top: -6px;
        position: absolute;
    }
</style>
@stop

@section('js')

@stop