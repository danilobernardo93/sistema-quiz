@extends('adminlte::page')

@section('title', 'Temas')

@section('content_header')
<h1>Temas</h1>
@stop

@section('content')
<div class="info-box row">
    @if ($message = Session::get('message'))
    <div class="alert alert-{{$message['type']}} alert-dismissible fade show w-100" role="alert"> {{$message['text']}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form method="post" enctype="multipart/form-data" action="{{route('painel.theme.store')}}" class="form-horizontal form-material row">
        @csrf
        <div class="form-group col-xs-12 col-md-7 col-lg-7">
            <label>Nome do tema </label>
            <div>
                <input class="form-control form-control-line" type="text" required="required" name="name">
            </div>
        </div>

        <div class="form-group col-xs-12 col-md-5 col-lg-5">
            <label>Tema está ativo ? </label>
            <div>
                <label>Não</label>
                <input type="range" min="0" max="1" step="1" name="active" style="width:50px">
                <label>Sim</label>
            </div>
        </div>

        <div class="form-group col-xs-12 col-md-12 col-lg-12">
            <label>Descrição</label>
            <div>
                <textarea class="form-control form-control" name="description"  maxlength="250"></textarea>
            </div>
        </div>

        <div class="form-group col-xs-12 col-md-12 col-lg-12">
            <label>Imagem</label>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <input type="file" class="form-control" name="image">
                </div>

                <div class="col-xs-12 col-md-6 col-lg-6">
                    Preview imagem
                </div>
            </div>
        </div>
        <div class="form-group col-md-12 col-lg-12">
            <input type="submit" value="Salvar" name="action" class="btn btn-success float-right" />
        </div>
    </form>
</div>
</div>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop