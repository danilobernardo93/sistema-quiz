@extends('adminlte::page')

<?php

use Illuminate\Support\Str;
?>

@section('title', 'Temas')

@section('content_header')
<h1>Temas</h1>
@stop

@section('content')
<div class="info-box row">
    <div class="col-md-12">
        <form action="{{ route('painel.theme.form') }}" method="post" class="row">
            @csrf
            <div class="form-group col-xs-8 col-md-4 col-lg-4">
                <label>Título</label>
                <div>
                    <input type="text" class="form-control" name="search" placeholder="Digite o nome do tema" />
                </div>
            </div>
            <div class="form-group col-xs-4 col-md-6 col-lg-6">
                <label></label>
                <div>
                    <input type="submit" class="btn btn-primary" name="action" value="Buscar" />
                </div>
            </div>
        </form>
        <hr>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="example2" class="table table-bordered table-hover no-wrap">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Descrição</th>
                        <th>Ativo</th>
                        <th>Editar</th>
                        <th>Deletar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($themes as $theme) : ?>
                        <tr>
                            <td><?= $theme->name ?></td>
                            <td><?= $theme->slug ?></td>
                            <td><?= Str::limit($theme->description, 50, '...') ?></td>
                            <td><?= $theme->active == 0 ? "Desativado" : "Ativo" ?></td>
                            <td><a href="{{URL::to('/painel/temas/editar/'.$theme->id)}}"><i class="fa fa-eye"></i></a></td>
                            <td><a href="#"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" style="text-align: right;font-weight:600"><?= count($themes) ?> temas localizados </td>
                    </tr>
                </tfoot>
            </table>
            <hr>
            {{ $themes->links() }}
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!');
</script>
@stop