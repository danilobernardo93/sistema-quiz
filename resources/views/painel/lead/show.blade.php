@extends('layouts.painel')

@section('content')

<div class="content-header sty-one">
    <h1>Lead</h1>
    <ol class="breadcrumb">
        <li><a href="{{URL::to('/painel/')}}">Home</a></li>
        <li><i class="fa fa-angle-right"></i> <a href="{{URL::to('/painel/leads')}}">Leads</a></li>
        <li><i class="fa fa-angle-right"></i> Visualização de lead</li>
    </ol>
</div>

@endsection()