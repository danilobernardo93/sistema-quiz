@extends('layouts.site')

@section('content')

<main id="general_page">
    <div class="container margin_60_35">
        <div class="main_title_2">
            <span><em></em></span>
            <h2><?=str_replace("-", " ",$tema)?></h2>
            <p>Selecione um quiz para iniciar</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/lavar-as-maos')}}">
                    <span><img src="{{asset('site/img/prevention_icon_1.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Lavar as mãos  <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/uso-de-mascara')}}">
                    <span><img src="{{asset('site/img/prevention_icon_2.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Uso de mascara <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/limpeza-com-alcool')}}">
                    <span><img src="{{asset('site/img/prevention_icon_3.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Limpeza com álcool <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/evite-aglomeracao')}}">
                    <span><img src="{{asset('site/img/prevention_icon_4.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite aglomeração <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/evite-locais-publicos')}}">
                    <span><img src="{{asset('site/img/prevention_icon_5.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite locais públicos <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="{{url('quiz/evite-tocar-em-coisas-compartilhadas')}}">
                    <span><img src="{{asset('site/img/prevention_icon_6.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite tocar em coisas compartilhadas <?=rand(10, 1000)?></h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
        </div>
    </div>
    <!-- /container -->
</main>

@endsection