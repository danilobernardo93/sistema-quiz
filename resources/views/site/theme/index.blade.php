@extends('layouts.site')

@section('content')

<main id="general_page">
    <div class="container margin_60_35">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>Selecione um tema</h2>
            <p></p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_1.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Lavar as mãos </h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_2.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Uso de mascara</h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_3.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Limpeza com álcool</h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_4.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite aglomeração</h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_5.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite locais públicos</h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <a class="box_topic" href="tema/nome-do-tema-aqui">
                    <span><img src="{{asset('site/img/prevention_icon_6.svg')}}" width="70" height="70" alt=""></span>
                    <h3>Evite tocar em coisas compartilhadas</h3>
                    <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris.</p>
                </a>
            </div>
        </div>
    </div>
    <!-- /container -->
</main>

@endsection