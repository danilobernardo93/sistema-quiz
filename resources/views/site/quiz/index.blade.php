@extends('layouts.site')

@section('content')

<div class="container">
    <div id="form_container">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div id="left_form">
                    <figure><img src="{{asset('site/img/info_graphic_1.svg')}}" alt="" width="100" height="100"></figure>
                    <h2><span>{{$quiz->name}}</span></h2>
                    <p>{{$quiz->description}}</p>
                    <!-- <a href="" class="btn_1 rounded yellow purchase" target="_parent">Purchase this template</a>
                        <a href="#wizard_container" class="btn_1 rounded mobile_btn yellow">Start Now!</a> -->
                    <a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>
                </div>
            </div>
            <div class="col-lg-8">
                <div id="wizard_container">
                    <div id="top-wizard">
                        <div id="progressbar"></div>
                        <span id="location"></span>
                    </div>
                    <form id="wrapped" action="{{route('site.quiz.form', $quiz->id)}}" method="post">
                        @csrf
                        <input id="website" name="website" type="text" value="">
                        <div id="middle-wizard">
                            <?php foreach ($questions as $line) :
                                $question = json_decode($line->question);
                                $alternative1 = json_decode($line->alternative1);
                                $alternative2 = json_decode($line->alternative2);
                                $alternative3 = json_decode($line->alternative3);
                                $alternative4 = json_decode($line->alternative4);
                            ?>
                                <div class="step">
                                    <h3 class="main_question"><i class="arrow_right"></i><?= $question->question ?></h3>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="container_radio version_2">{{$alternative1->alternative}}
                                                    <input type="radio" name="question_{{$line->id}}" value="alternative1" class="required">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="container_radio version_2">{{$alternative2->alternative}}
                                                    <input type="radio" name="question_{{$line->id}}" value="alternative2" class="required">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="container_radio version_2">{{$alternative3->alternative}}
                                                    <input type="radio" name="question_{{$line->id}}" value="alternative3" class="required">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="container_radio version_2">{{$alternative4->alternative}}
                                                    <input type="radio" name="question_{{$line->id}}" value="alternative4" class="required">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /row -->
                                </div>
                            <?php endforeach; ?>
                            <div class="submit step" id="end">
                                <h3 class="main_question"><i class="arrow_right"></i>Por favor, preencha com seus dados abaixo
                                    data</h3>
                                <div class="form-group add_top_30">
                                    <label for="name">Nome</label>
                                    <input type="text" name="name" id="name" class="form-control required" onchange="getVals(this, 'name_field');">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control required" onchange="getVals(this, 'email_field');">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Telefone</label>
                                    <input type="text" name="phone" id="phone" class="form-control required">
                                </div>
                                <div class="text-center">
                                    <div class="form-group terms">
                                        <label class="container_check"><small>Aceite nosso <a href="#" data-toggle="modal" data-target="#terms-txt">Termos e Condições</a> antes de enviar.</small>
                                            <input type="checkbox" name="terms" value="sim" class="required" checked>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /middle-wizard -->
                        <div id="bottom-wizard">
                            <button type="button" name="backward" class="backward">Voltar</button>
                            <button type="button" name="forward" class="forward">Próximo</button>
                            <button type="submit" name="process" class="submit">Enviar</button>
                        </div>
                        <!-- /bottom-wizard -->
                    </form>
                </div>
                <!-- /Wizard container -->
            </div>
        </div><!-- /Row -->
    </div><!-- /Form_container -->
</div>

<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos de uso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>,
                    mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus,
                    pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum
                    accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum
                    sanctus, pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="more-info" tabindex="-1" role="dialog" aria-labelledby="more-infoLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="more-infoLabel">Informação importante</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>,
                    mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus,
                    pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum
                    accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum
                    sanctus, pro ne quod dicunt sensibus.</p>
                <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam
                    dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt
                    sensibus.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection