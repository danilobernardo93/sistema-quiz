<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Magnifica Questionnaire Form Wizard includes Coronavirus Health questionnaire">
    <meta name="author" content="Ansonika">
    <title>Quiz | Página Inicial</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('site/img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('site/img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('site/img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('site/img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('site/img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('site/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/menu.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('site/css/vendors.css')}}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('site/css/custom.css')}}" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="{{asset('site/js/modernizr.js')}}"></script>

</head>

<body>

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>

    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <a href="{{route('site.home')}}"><img src="{{asset('site/img/logo.svg')}}" alt="" width="178" height="45" class="d-none d-md-block"><img src="{{asset('site/img/logo_mobile.svg')}}" alt="" width="62" height="45" class="d-block d-md-none"></a>
                </div>
                <div class="col-9">
                    <div id="social">
                        <ul>
                            <li><a href="#0"><i class="icon-facebook"></i></a></li>
                            <li><a href="#0"><i class="icon-twitter"></i></a></li>
                            <li><a href="#0"><i class="icon-google"></i></a></li>
                            <li><a href="#0"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
                    <nav>
                        <ul class="cd-primary-nav">
                            <li><a href="{{route('site.home')}}" class="animated_link">Home</a></li>
                            <li><a href="{{route('site.temas')}}" class="animated_link">Temas</a></li>
                            <li><a href="{{route('site.contato')}}" class="animated_link">Contato</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="footer_in clearfix">
        <div class="container">
            <p>© <?=date("Y")?> Todos os direitos reservados.</p>
            <ul>
                <li><a href="{{route('site.home')}}" class="animated_link">Home</a></li>
                <li><a href="{{route('site.temas')}}" class="animated_link">Temas</a></li>
                <li><a href="{{route('site.contato')}}" class="animated_link">Contato</a></li>
            </ul>
        </div>
    </footer>

    <div class="cd-overlay-nav">
        <span></span>
    </div>

    <div class="cd-overlay-content">
        <span></span>
    </div>

    <!-- COMMON SCRIPTS -->
    <script src="{{asset('site/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('site/js/common_scripts.min.js')}}"></script>
    <script src="{{asset('site/js/velocity.min.js')}}"></script>
    <script src="{{asset('site/js/common_functions.js')}}"></script>

    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('site/js/func_1.js')}}"></script>
    <script src="{{asset('site/js/parallax.min.js')}}"></script>
</body>

</html>