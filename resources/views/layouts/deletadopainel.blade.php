<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Painel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

    <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('painel/bootstrap/css/bootstrap.min.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('painel/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('painel/css/themify-icons/themify-icons.css')}}">

    <!-- Chartist CSS -->
    <link rel="stylesheet" href="{{asset('painel/plugins/chartist-js/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('painel/plugins/chartist-js/chartist-plugin-tooltip.css')}}">

    <meta name="url_admin" content="{{URL::to('/painel/')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="sidebar-mini">
    <div class="wrapper boxed-wrapper">
        <header class="main-header">
            <a href="{{URL::to('/painel/')}}" class="logo blue-bg">
                <span class="logo-mini"><img src="{{asset('painel/img/logo_branco.png')}} ?>" height="50px"></span>
                <span class="logo-lg"><img src="{{asset('painel/img/logo.png')}} ?>" height="50px"></span>
            </a>
            <nav class="navbar blue-bg navbar-static-top">
                <ul class="nav navbar-nav pull-left">
                    <li><a class="sidebar-toggle" data-toggle="push-menu" href=""></a> </li>
                </ul>
            </nav>
        </header>
        <aside class="main-sidebar">
            <div class="sidebar">
                <div class="user-panel">
                    <div class="info">
                        <p>Danilo</p>
                        <a href="#" title="Sair"><i class="fa fa-power-off"></i></a>
                    </div>
                </div>

                <ul class="sidebar-menu" data-widget="tree">
                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Quiz</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Lista de Quiz</a></li>
                            <li><a href="#">Adicionar Quiz</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Leads</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <hr>

                    <li class="treeview "> <a href="#"> <i class="fa fa-dashboard"></i> <span>Temas</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="{{URL::to('/painel/temas')}}">Lista de temas</a></li>
                            <li><a href="{{URL::to('/painel/temas/novo')}}">Adicionar tema</a></li>
                        </ul>
                    </li>

                    <li class="treeview "> <a href="#"> <i class="fa fa-dashboard"></i> <span>Usuários</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Lista de leads</a></li>
                            <li><a href="#">Lista de usuários do sistema</a></li>
                        </ul>
                    </li>

                    

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>

                    <li class="treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Item ADM GERAL</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                        <ul class="treeview-menu">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Lista de leads</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- jQuery 3 -->
        <script src="{{asset('painel/js/jquery.min.js')}}"></script>

        <!-- v4.0.0-alpha.6 -->
        <script src="{{asset('painel/bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- template -->
        <script src="{{asset('painel/js/niche.js')}}"></script>

        <!-- Chartjs JavaScript -->
        <script src="{{asset('painel/plugins/chartjs/chart.min.js')}}"></script>
        <script src="{{asset('painel/plugins/chartjs/chart-int.js')}}"></script>
        <script src="{{asset('painel/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <!-- Chartist JavaScript -->
        <script src="{{asset('painel/plugins/chartist-js/chartist.min.js')}}"></script>
        <script src="{{asset('painel/plugins/chartist-js/chartist-plugin-tooltip.js')}}"></script>
        <script src="{{asset('painel/plugins/functions/chartist-init.js')}}"></script>
    </div>
</body>

</html>