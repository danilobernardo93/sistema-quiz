<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'question',
        'alternative1',
        'alternative2',
        'alternative3',
        'alternative4',
        'quiz_id',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}
