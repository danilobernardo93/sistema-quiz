<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizPersonalization extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'quiz_id',
        'backgroundImage',
        'textBtnSendForm',
        'colorBtnSendForm',
        'backgroundBtnSendForm',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}
