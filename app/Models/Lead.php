<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'quiz_id',
        'name',
        'email',
        'phone',
        'terms',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }

    public function quizAnswer()
    {
        return $this->belongsTo(quizAnswer::class);
    }

}
