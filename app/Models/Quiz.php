<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quiz extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'name',
        'description',
        'slug',
        'active',
        'image',
        'show_in_site',
        'theme_id',
        'user_id',
        'pixel',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->hasMany(Question::class);
    }

    public function result()
    {
        return $this->hasMany(Result::class);
    }

    public function lead()
    {
        return $this->hasMany(Lead::class);
    }

    public function quizPermission()
    {
        return $this->hasMany(QuizPersonalization::class);
    }
}
