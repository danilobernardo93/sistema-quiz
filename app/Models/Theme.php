<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Theme extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'image',
        'active',
        'slug'
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }
}
