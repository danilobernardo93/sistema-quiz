<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizAnswer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'lead_id',
        'historic',
        'totalWeight',
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }
}
