<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Models\Question;
use App\Models\Lead;
use App\Models\QuizAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }



    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if (empty($slug)) {
            return redirect();
        }

        $quiz = Quiz::where('slug', '=', $slug)->get()[0];

        if (empty($quiz)) {
            return redirect();
        }
        $questions = Question::where('quiz_id', '=', $quiz->id)->get();
        // var_dump($questions);die();

        return view('site.quiz.index', ['quiz' => $quiz, 'questions' => $questions]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function form(Request $request, $quiz_id)
    {
        //All questions answereds
        $questionsAnswereds = $request->all();

        // Fin all questions the of QUIZ answered
        $questionsQuiz = Question::where('quiz_id', '=', $quiz_id)->get();

        $listQuestions = [];
        $alternatives  = [];

        foreach ($questionsQuiz as $line) {
            $listQuestions[] = 'question_' . $line->id;
            $alternatives['question_' . $line->id] =
                [
                    'question'     => json_decode($line->question)->question,
                    'alternative1' => json_decode($line->alternative1),
                    'alternative2' => json_decode($line->alternative2),
                    'alternative3' => json_decode($line->alternative3),
                    'alternative4' => json_decode($line->alternative4),
                ];
        }

        //Verify if questions the of form exist in database
        $weightForm = 0;
        $historicQuiz = [];
        foreach ($questionsAnswereds as $q => $r) {
            if (in_array($q, $listQuestions)) {
                $weightForm += $alternatives[$q][$r]->weight;
                $historicQuiz[] = [
                    'question' =>  $alternatives[$q]['question'], 
                    'answerSelected' =>  $alternatives[$q][$r]->alternative,
                    'answerWeight' =>  $alternatives[$q][$r]->weight,
                ];
            }
        }

        $lead = [
            'quiz_id' => $quiz_id,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'terms' => true,
        ];

        //Save the new lead
        if($lead = Lead::create($lead)){
            $historicLead = [
                'lead_id' => $lead->id,
                'historic' => json_encode($historicQuiz),
                'totalWeight' => $weightForm
            ];

            //Save the history of the quiz
            QuizAnswer::create($historicLead);
        }
        return redirect('quiz/finalizado/' . base64_encode($quiz_id) . '&' . base64_encode($weightForm));
    }


    public function thanks($hash)
    {
        if (empty($hash)) {
            return redirect('/');
        }

        $hash = explode("&", $hash);

        if (count($hash) !=2) {
            return redirect('/');
        }
        
        $quiz_id     = intval(base64_decode($hash[0]));
        $weightForm = intval(base64_decode($hash[1]));
        
        if (!$quiz = Quiz::find($quiz_id)) {
            return redirect('/');
        }

        if (!$result = DB::select("SELECT * FROM results r WHERE quiz_id = '{$quiz_id}' AND '{$weightForm}' BETWEEN r.from AND r.to")[0]) {
            return redirect('/');
        }

        return view('site.quiz.thanks', ['quiz' => $quiz, 'messageReturn' => $result]);
    }
}
