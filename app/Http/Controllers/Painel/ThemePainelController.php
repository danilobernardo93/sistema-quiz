<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Pagination\Paginator;


class ThemePainelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $themes  = Theme::latest()->paginate(1);
        return view('painel.theme.index', ["themes" => $themes]);
    }

    public function form(Request $request)
    {
        Paginator::useBootstrap();
        $themes  = Theme::where("name", "LIKE", "%$request->search%")->paginate(1);
        return view('painel.theme.index', compact("themes"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.theme.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required|string',
            'description' => 'required|string',
            'active' => 'required|boolean',
        ]);
        $imageName = time() . '.' . $request->image->extension();
        $slug = Str::slug($request->name, '-');
        $data['name'] = $request->name;
        $data['slug'] = $slug;
        $data['description'] = $request->description;
        $data['active'] = $request->active;
        if (!empty($request->image)) {
            $data['image'] = $imageName;
        }

        DB::beginTransaction();

        if (Theme::create($data)) {
            if (!empty($request->image)) {
                if ($request->image->move(public_path('site/img/theme'), $imageName)) {
                    DB::commit();
                    return back()->with('message', [
                        "type" => "success",
                        "text" => "Tema $request->name salvo com sucesso!",
                    ]);
                }
            } else {
                DB::commit();
                return back()->with('message', [
                    "type" => "success",
                    "text" => "Tema $request->name salvo com sucesso!",
                ]);
            }
        }

        DB::rollBack();
        return back()->with('message', [
            "type" => "warning",
            "text" => "Erro ao salvar, tente novamente.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $theme = Theme::find($id);
        return view('painel.theme.show', ["theme" => $theme]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $errorMessage = null;
        if ($theme = Theme::find($id)) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'name' => 'required|string',
                'description' => 'required|string',
                'active' => 'required|boolean',
            ]);

            if (!empty($request->image)) {
                $imgDelete = $theme->image;
                $imageName = time() . '.' . $request->image->extension();
                $data['image'] = $imageName;

                if (!$request->image->move(public_path('site/img/theme'), $imageName)) {
                    $errorMessage = [
                        "type" => "warning",
                        "text" => "Erro ao realizar upload da imagem, tente novamente.",
                    ];
                }

                if(empty($errorMessage)){
                    if (file_exists(public_path('site/img/theme/') . $imgDelete)) {
                        unlink(public_path('site/img/theme/') . $imgDelete);
                    }
                }
            }

            $data['name'] = $request->name;
            $data['slug'] = Str::slug($request->name, '-');
            $data['active'] = $request->active;
            $data['description'] = $request->description;


            DB::beginTransaction();

            
            if(empty($errorMessage)){
                if ($theme->update($data)) {
                    DB::commit();
                    return back()->with('message', [
                        "type" => "success",
                        "text" => "Tema atualizado com sucesso!",
                    ]);
                }else{
                    $errorMessage = [
                        "type" => "warning",
                        "text" => "Erro ao atualizar, tente novamente.",
                    ];
                }
            }

            DB::rollBack();
            return back()->with('message', $errorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
