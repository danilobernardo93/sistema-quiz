<?php

namespace App\Http\Controllers\Painel;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\Result;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Theme;
use App\Models\User;
use Illuminate\Pagination\Paginator;
use Symfony\Component\Console\Input\StringInput;

class QuizPainelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $quizzes  = Quiz::join('themes', 'themes.id', '=', 'quizzes.theme_id')
            ->select('quizzes.*', 'themes.name as theme')
            ->latest('quizzes.created_at')->paginate(2);

        return view('painel.quiz.index', ["quizzes" => $quizzes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = Theme::get("*");
        return view('painel.quiz.create', ['themes' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'theme' => 'required|',
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        $user = auth()->user();
        $imageName = time() . '.' . $request->image->extension();
        $slug = Str::slug($request->name, '-');
        $data['name'] = $request->name;
        $data['theme'] = $request->theme;
        $data['slug'] = $slug;
        $data['description'] = $request->description;
        $data['active'] = $request->active;
        $data['theme_id'] = $request->theme;
        $data['show_in_site'] = $request->show_in_site;
        $data['user_id'] = $user->id;
        $data['pixel'] = $request->pixel;

        if (!empty($request->image)) {
            $data['image'] = $imageName;
        }

        DB::beginTransaction();

        if ($quiz = Quiz::create($data)) {
            if (!empty($request->image)) {
                if ($request->image->move(public_path('site/img/quiz'), $imageName)) {
                    DB::commit();
                    back()->with('message', [
                        "type" => "success",
                        "text" => "Quiz salvo com sucesso!",
                    ]);
                    return redirect('painel/quiz/editar/' . $quiz->id);
                }
            } else {
                DB::commit();
                back()->with('message', [
                    "type" => "success",
                    "text" => "Quiz salvo com sucesso!",
                ]);
                return redirect('painel/quiz/editar/' . $quiz->id);
            }
        }

        DB::rollBack();
        return back()->with('message', [
            "type" => "warning",
            "text" => "Erro ao salvar, tente novamente.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!$quiz = Quiz::find($id)) {
            return 'Redirecionar';
        }
        $themes = Theme::get("*");
        $questions = Question::where("quiz_id", "=", $id)->get();
        $results = Result::where("quiz_id", "=", $id)->get();
        return view('painel.quiz.show', ['themes' => $themes, 'quiz' => $quiz, 'questions' => $questions, 'results' => $results]);
    }

    //Busca quiz 
    public function searchQuizByName(Request $request)
    {
        Paginator::useBootstrap();

        $quizzes  = Quiz::join('themes', 'themes.id', '=', 'quizzes.theme_id')
            ->select('quizzes.*', 'themes.name as theme')
            ->where("quizzes.name", "LIKE", "%$request->search%")
            ->latest('quizzes.created_at')->paginate(2);

        return view('painel.quiz.index', ["quizzes" => $quizzes]);
    }

    public function update(Request $request, $id)
    {
        switch ($request->action) {
            case 'Adicionar pergunta':
                return $this->createQuestion($request, $id);
                break;

            case 'Atualizar quiz':
                return $this->updateQuiz($request, $id);
                break;

            case 'Atualizar quiz':
                return $this->updateQuiz($request, $id);
                break;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateQuiz(Request $request, $id)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'theme' => 'required|',
            'name' => 'required|string',
            'description' => 'required|string',
        ]);


        $message = null;
        if ($quiz = Quiz::find($id)) {

            $slug = Str::slug($request->name, '-');
            $data['name'] = $request->name;
            $data['theme'] = $request->theme;
            $data['slug'] = $slug;
            $data['description'] = $request->description;
            $data['active'] = $request->active;
            $data['theme_id'] = $request->theme;
            $data['show_in_site'] = $request->show_in_site;
            $data['user_id'] = $quiz->user_id;
            $data['pixel'] = $request->pixel;

            if (!empty($request->image)) {
                $imageName = time() . '.' . $request->image->extension();
                $data['image'] = $imageName;
            }

            DB::beginTransaction();

            if (!empty($request->image)) {
                if (!$request->image->move(public_path('site/img/quiz/'), $imageName)) {
                    $message = [
                        "type" => "warning",
                        "text" => "Erro no upload da imagem.",
                    ];
                }

                if (empty($message)) {
                    if (file_exists(public_path('site/img/quiz/') . $quiz->image)) {
                        unlink(public_path('site/img/quiz/') . $quiz->image);
                    }
                }
            }

            if (empty($message)) {
                if (!$quiz->update($data)) {
                    $message = [
                        "type" => "warning",
                        "text" => "Erro ao atualizar o quiz.",
                    ];
                } else {



                    DB::commit();
                    back()->with('message', [
                        "type" => "success",
                        "text" => "Quiz atualizado com sucesso.",
                    ]);
                    return redirect('painel/quiz/editar/' . $quiz->id);
                }
            }

            DB::rollBack();
            return back()->with('message', $message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //QUESTIONS 

    public function findQuestionById(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return 'ID question required';
        }

        if (!$question = Question::find($id)) {
            return 'Questão não localizada';
        }

        return $question;
    }

    /**
     * Create question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $quizId
     * @return \Illuminate\Http\Response
     */
    public function createQuestion(Request $request, $quizId)
    {
        $data['question'] = json_encode(['question' => $request->question]);
        $data['alternative1'] = json_encode(['alternative' => $request->alternative1, 'weight' => $request->weight1]);
        $data['alternative2'] = json_encode(['alternative' => $request->alternative2, 'weight' => $request->weight2]);
        $data['alternative3'] = json_encode(['alternative' => $request->alternative3, 'weight' => $request->weight3]);
        $data['alternative4'] = json_encode(['alternative' => $request->alternative4, 'weight' => $request->weight4]);
        $data['quiz_id']  = $quizId;

        // dd($data);

        DB::beginTransaction();

        if ($Question = Question::create($data)) {
            DB::commit();
            back()->with('message', [
                "type" => "success",
                "text" => "Questão criada com sucesso.",
            ]);
            return redirect('painel/quiz/editar/' . $quizId);
        }

        DB::rollBack();
        return back()->with('message', [
            "type" => "warning",
            "text" => "Erro ao criar questão, tente novamente.",
        ]);
    }

    /**
     * Update question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $idQuestion
     * @return \Illuminate\Http\Response
     */
    public function updateQuestion(Request $request, $idQuestion)
    {
        $message = [];

        if (!$Question = Question::find($idQuestion)) {
            $message = [
                "type" => "warning",
                "text" => "Pergunta não encontrada, tente novamente.",
            ];
        }

        $data['question'] = json_encode(['question' => $request->question]);
        $data['alternative1'] = json_encode(['alternative' => $request->alternative1, 'weight' => $request->weight1]);
        $data['alternative2'] = json_encode(['alternative' => $request->alternative2, 'weight' => $request->weight2]);
        $data['alternative3'] = json_encode(['alternative' => $request->alternative3, 'weight' => $request->weight3]);
        $data['alternative4'] = json_encode(['alternative' => $request->alternative4, 'weight' => $request->weight4]);


        DB::beginTransaction();
        if (!$Question->update($data)) {
            $message = [
                "type" => "warning",
                "text" => "Erro ao atualizar a pergunta, tente novamente.",
            ];
        }

        if (empty($message)) {
            DB::commit();
            $message = [
                "type" => "success",
                "text" => "Pergunta atualizada com sucesso.",
            ];
        } else {
            DB::rollBack();
        }

        return back()->with('message', $message);
    }

    //RESULTS 

    public function findResultById(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return 'Informar ID';
        }

        if (!$result = Result::find($id)) {
            return 'Resultado não localizado';
        }

        return $result;
    }

    /**
     * Create condition result in the Quiz
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $quizId
     * @return \Illuminate\Http\Response
     */
    public function createResult(Request $request, $quizId)
    {
        $data['from'] = $request->from;
        $data['to'] = $request->to;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['textButton'] = $request->textButton;
        $data['linkButton'] = $request->linkButton;
        $data['colorButton'] = $request->colorButton;
        $data['backgroundButton'] = $request->backgroundButton;
        $data['version'] = 1;
        $data['quiz_id'] = $quizId;

        DB::beginTransaction();

        if ($Result = Result::create($data)) {
            DB::commit();
            back()->with('message', [
                "type" => "success",
                "text" => "Resultado criado com sucesso.",
            ]);
            return redirect('painel/quiz/editar/' . $quizId);
        }

        DB::rollBack();
        return back()->with('message', [
            "type" => "warning",
            "text" => "Erro ao criar resultado, tente novamente.",
        ]);
    }

    /**
     * Create condition result in the Quiz
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $quizId
     * @return \Illuminate\Http\Response
     */
    public function updateResult(Request $request, $resultId)
    {
        if (!$Result = Result::find($resultId)) {
            return back()->with('message', [
                "type" => "warning",
                "text" => "Resultado não encontrado.",
            ]);
        }
        $data['from'] = $request->from;
        $data['to'] = $request->to;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['textButton'] = $request->textButton;
        $data['linkButton'] = $request->linkButton;
        $data['colorButton'] = $request->colorButton;
        $data['backgroundButton'] = $request->backgroundButton;
        $data['version'] = 1;
        $data['quiz_id'] = $Result->quiz_id;

        DB::beginTransaction();

        if ($Result->update($data)) {
            DB::commit();
            $message = [
                "type" => "success",
                "text" => "Resultado atualizado com sucesso.",
            ];
        } else {
            DB::rollBack();
            $message =  [
                "type" => "warning",
                "text" => "Erro ao criar resultado, tente novamente.",
            ];
        }

        return back()->with('message', $message);
    }
}
